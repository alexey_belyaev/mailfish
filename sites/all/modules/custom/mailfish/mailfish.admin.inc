<?php
// $Id$

/**
* Build and return the mailfish settings form
*/
function mailfish_admin_settings() {
  $types = node_type_get_names();
  $form['mailfish_allowed_content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed content types'),
    '#options' => $types,
    '#default_value' => variable_get('mailfish_allowed_content_type', array()),
    '#description' => t('Chose content types that will be allowed to subscribe.'),
    '#required' => FALSE,
  );
  $form['markup'] = array(
    '#markup' => '<fieldset style="padding:1em"><legend>Content type - path relationships</legend>',
  );
  $content_type_path = variable_get('content_type_path', array());
  foreach ($types as $key => $value) {
    $form[$key] = array(
      '#type' => 'textfield',
      '#title' => $value,
      '#default_value' => (isset($content_type_path[$key])) ? $content_type_path[$key] : '',
      '#required' => FALSE,
      '#description' => 'enter path of the node list of this type, without first and last slashes',
      '#states' => array(
        'invisible' => array(
          ':input[name="mailfish_allowed_content_type['.$key.']"]' => array(
            'checked' => FALSE
          )
        ),
      ),
    );
  }
  $form['markup1'] = array(
    '#markup' => '</fieldset>',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#submit'][] = 'mailfish_settings_form_submit';
  return $form;
}


/**
* Implements hook_settings_form_submit().
*/
function mailfish_settings_form_submit($form, &$form_state) {
  $types = node_type_get_names();
  form_state_values_clean($form_state);
  $content_type_path = array();
  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    if(in_array($key, array_flip($types), TRUE)){
      $content_type_path[$key] = $value;
    }else
      variable_set($key, $value);
    if(isset($content_type_path))
      variable_set('content_type_path', $content_type_path);
  }
    // die();

  drupal_set_message(t('The configuration options have been saved.'));
}