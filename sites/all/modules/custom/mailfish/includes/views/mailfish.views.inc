<?php

/**
* Implements hook_views_data_alter().
*/
function mailfish_views_data_alter(&$data) {
  $data['mailfish_mails']['created_value']['filter']['handler'] = 'views_handler_filter_date';
  // print_r($data);
 return $data;
}


/**
 * Implements hook_views_data()
 */
function mailfish_views_data() {

  $table = array(

//subscribe mailfish_mails table
    'mailfish_mails' => array(
     'table' => array(
        'group' => t('Mailfish'),
        'base' => array(
          'sid' => 'node subscribe id',
          'title' => t('Mailfish node subscribtion'),
          'help' => t('Content from mailfish subscribtion for node.'),
         )
      ),

      //Description of sid
      'sid' => array(
        'title' => t('Mailfish Custom Id'),
        'help' => t('Mailfish Custom table Id field'),
        // is mydons_custom_id field sortable TRUE
        'field' => array(
          'click sortable' => TRUE,  
        ),
        //Filter handler for filtering records by sid
        'filter' => array(
           'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
       ),

      //Description of uid field
      'uid' => array(
        'title' => t('User Id'),
        'help' => t('User Id'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        ),
        'relationship'=> array(
          'base' => 'users',
          'base field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('User ID relationship'),
          'title' => t('User ID relationship'),
          'help' => t('User ID relationship'),
        )
      ),

      //Description of mail field
      'mail' => array(
        'title' => t('User custom mail'),
        'help' => t('Custom mail that user enter to subscribe for node (not the registration mail)'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        ),
      ),

     //Description of nid field
      'nid' => array(
        'title' => t('Node id'),
        'help' => t('Node id'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
           'handler' => 'views_handler_sort'
        ),
        'relationship'=> array(
          'base' => 'node',
          'base field' => 'nid',
          'handler' => 'views_handler_relationship',
          'label' => t('Node ID relationship'),
          'title' => t('Node ID relationship'),
          'help' => t('Node ID relationship'),
        )
      ),

     //Description of date field
      'created' => array(
        'title' => t('Date'),
        'help' => t('Node subscribe created date'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date'
        )
      ),
    ),



//subscribe mailfish_mails_ct table
    'mailfish_mails_ct' => array(
     'table' => array(
        'group' => t('Mailfish'),
        'base' => array(
          'sid' => 'content type subscribe id',
          'title' => t('Mailfish content type subscribtion'),
          'help' => t('Content from mailfish subscribtion for content type.'),
         )
      ),

      //Description of sid
      'sid' => array(  
        'title' => t('Mailfish Custom Id'),
        'help' => t('Mailfish Custom table Id field'),
        // is mydons_custom_id field sortable TRUE
        'field' => array(
          'click sortable' => TRUE,  
        ),
        //Filter handler for filtering records by sid
        'filter' => array(
           'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
       ),

      //Description of uid field
      'uid' => array(
        'title' => t('User Id'),
        'help' => t('User Id'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        ),
        'relationship'=> array(
          'base' => 'users',
          'base field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('User ID relationship'),
          'title' => t('User ID relationship'),
          'help' => t('User ID relationship'),
        )
      ),

      //Description of mail field
      'mail' => array(
        'title' => t('User custom mail'),
        'help' => t('Custom mail that user enter to subscribe for content type (not the registration mail)'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        ),
      ),

     //Description of nid field
      'ctid' => array(
        'title' => t('Content type machine name'),
        'help' => t('Content type machine name'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
           'handler' => 'views_handler_sort'
        ),
        'relationship'=> array(
          'base' => 'node_type',
          'base field' => 'orig_type',
          'handler' => 'views_handler_relationship',
          'label' => t('Content type ID relationship'),
          'title' => t('Content type ID relationship'),
          'help' => t('Content type ID relationship'),
        )
      ),

     //Description of date field
      'created' => array(
        'title' => t('Date'),
        'help' => t('Content type subscribe created date'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date'
        )
      ),
    )
  );
  return $table;
}
